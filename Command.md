***********************************************************
Client Source Code
***********************************************************
git clone https://gitlab.com/kiocjw/kaga-angular-8.git kaga
cd kaga

***********************************************************
Client Release Distribution
***********************************************************
git clone https://gitlab.com/kiocjw/kaga-angular-8-dist.git dist
cd dist

***********************************************************
Client Release Command
***********************************************************
git pull
git rm *
cd ..
npm run build --prod
cd dist
git add *
git commit -m "Fixed Release"
git push