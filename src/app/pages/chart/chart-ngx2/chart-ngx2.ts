import { Component, Injectable, OnInit } from '@angular/core';
import { NgbDateAdapter, NgbDateStruct,NgbDateParserFormatter} from '@ng-bootstrap/ng-bootstrap';
import { TableData } from './data';

import * as global from '../../../config/globals';
import { Observable } from 'rxjs/Rx';
import { DatePipe } from '@angular/common';
//import {Parser} from 'xml2js';
import {Mutex, MutexInterface, Semaphore, SemaphoreInterface, withTimeout} from 'async-mutex';
const mutex1 = new Mutex();
const mutex2 = new Mutex();
import { HttpClient } from '@angular/common/http';
// { Http, Response,Headers, RequestOptions} from '@angular/http';
import { forkJoin } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { isUndefined } from 'util';
import { Result } from '../../../models/result'
import {Parser} from 'xml2js';
// for ngb datepicker adapter
@Injectable()
export class CustomDate2ParserFormatter extends NgbDateParserFormatter {

  readonly DELIMITER = '/';

  parse(value: string): NgbDateStruct | null {
    if (value) {
      let date = value.split(this.DELIMITER);
      return {
        day : parseInt(date[0], 10),
        month : parseInt(date[1], 10),
        year : parseInt(date[2], 10)
      };
    }
    return null;
  }

  format(date: NgbDateStruct | null): string {
    return date ? date.day + this.DELIMITER + date.month + this.DELIMITER + date.year : '';
  }
}

@Component({
  selector: 'chart-ngx2',
  templateUrl: './chart-ngx2.html',
  providers: [{provide: NgbDateParserFormatter, useClass: CustomDate2ParserFormatter}]
})

export class ChartNgx2Page implements OnInit {
  

  global = global;
  isDB = true;
  isLive = true;
  tdelay = 5000;
  //isRunning1 = false;
  //isRunning2 = false;
  // ngbdatepicker
  modelDate01: any;
  modelDate02: any;
  get today() {
    return new Date();
  }

  total:number;
  jobstotal:number;
  salestotal:number;
  five:number;
  six:number;
  seven:number;
  eight:number;
  nine:number;

  async trigger()
  {

    await mutex1.runExclusive(async () => {
      try {
          this.LoadJob();
      } finally {
          // Please read and understand the WARNING above before using this API.
      }
    });

    await mutex2.runExclusive(async () => {
      try {
          this.LoadJobsAchievement();
      } finally {
          // Please read and understand the WARNING above before using this API.
      }
    });
    
  } 

  //FromDate change//
  onDate1Selected(item: any)
  {
    //console.log(item);
    this.trigger();

  }
  //ToDate change//
  onDate2Selected(item: any)
  {
    //console.log(item);
    this.trigger();
  }

  ngOnInit() {
    let dateParts = this.datepipe.transform(this.today, 'yyyy-MM-dd').split('-');
    this.modelDate01 = { year: parseInt(dateParts[0],10), month: parseInt(dateParts[1],10), day: parseInt(dateParts[2],10) };
    this.modelDate02 = { year: parseInt(dateParts[0],10), month: parseInt(dateParts[1],10), day: parseInt(dateParts[2],10) };
    this.onChangeTable(this.config);
  }

  public rows:Array<any> = [];
  public columns:Array<any> = [
    {
      title: 'Model', name: 'model', sort: '', 
      filtering: {filterString: '', placeholder: 'Filter by model'}
    },
    {
      title: 'Job', name: 'job', sort: '', 
      filtering: {filterString: '', placeholder: 'Filter by job'}
    },
    {
      title: 'Job & QTY', name: 'qty', sort: '', 
      //filtering: {filterString: '', placeholder: 'Filter by extn.'}
    }
    ,
    {
      title: 'Input', name: 'input', sort: '', 
      //filtering: {filterString: '', placeholder: 'Filter by extn.'}
    }
    ,
    {
      title: 'Output', name: 'output', sort: '', 
      //filtering: {filterString: '', placeholder: 'Filter by extn.'}
    }
    ,
    {
      title: 'NG Sets', name: 'ngset', sort: '', 
      //filtering: {filterString: '', placeholder: 'Filter by extn.'}
    }
  ];

  public page:number = 1;
  public itemsPerPage:number = 6;
  public maxSize:number = 4;
  public numPages:number = 1;
  public length:number = 0;

  public config:any = {
    paging: true,
    sorting: {columns: this.columns},
    filtering: {filterString: ''},
    className: ['table-striped', 'table-bordered', 'm-b-0']
  };

  private data:Array<any> = TableData;
  baseURL:string;
  public constructor(public datepipe: DatePipe, private http:HttpClient) { 
    this.baseURL= "http://localhost:8008";

    this.length = this.data.length;
    let dateParts = this.datepipe.transform(this.today, 'yyyy-MM-dd').split('-');
    this.modelDate01 = { year: parseInt(dateParts[0],10), month: parseInt(dateParts[1],10), day: parseInt(dateParts[2],10) };
    this.modelDate02 = { year: parseInt(dateParts[0],10), month: parseInt(dateParts[1],10), day: parseInt(dateParts[2],10) };
    this.trigger();

    Observable.interval(this.tdelay).subscribe(x => { // will execute every 5 
      if (this.isLive)
      {
        if (this.isDB)
        {
          let that = this;
          this.getDB()
          .then(data => {
            that.trigger();
  
          });
        }
        else
        {
          this.trigger();
        }
      }
     });
     
  }

  private promisesParser(string)
  {
      var xml2jsParser = require('xml2js').parseString
      return new Promise(function(resolve, reject)
      {
        xml2jsParser(string, function(err, result) {
              if (err) {
                  return reject(err);
               } else {
                  return resolve(result);
               }
          });
      });
  }

  private convert(n) {
    n = String(n)
    if (n.length == 1)
      n = '0' + n
    return n
  }

  private LoadJob()
  {
    let parser = require('xml2js').Parser();
    this.getAllJobs()
    .then(jobdata => {
        //console.log(jobdata);
        let that = this;
        this.promisesParser(jobdata).then(function(r){
          that.total = Number(r['row']['TOTAL']);
          that.jobstotal = Number(r['row']['SUBTOTAL']);
          that.five = r['row']['FIVE'];
          that.six = r['row']['SIX'];
          that.seven = r['row']['SEVEN'];
          that.eight = r['row']['EIGHT'];
          that.nine = r['row']['NINE'];
          mutex1.release();
        }).catch(function(e){
          mutex1.release();
          throw new Error(e) 
        });
    })
    .catch(err => {
      mutex1.release();
       throw new Error(err) 
    });

    this.getAllSales()
    .then(data => {
        //console.log(data);
        let that = this;
        this.promisesParser(data).then(function(r){
          that.salestotal = Number(r['row']['SALES']);
        }).catch(function(e){
          throw new Error(e) 
        });
    })
    .catch(err => {
       throw new Error(err) 
    });
  }
  
  private LoadJobsAchievement()
  {
    let fromdatemodel = this.modelDate01;
    let fromdatestr = fromdatemodel['year']+"-"+this.convert(fromdatemodel['month'])+"-"+this.convert(fromdatemodel['day']);
    let todatemodel = this.modelDate02;
    let todatestr = todatemodel['year']+"-"+this.convert(todatemodel['month'])+"-"+this.convert(todatemodel['day']);
    
    let fromdate = new Date(fromdatestr);
    let todate = new Date(todatestr);
    
    this.getAllJobsAchievement(fromdatestr,todatestr)
    .then(data => {
        let that = this;
        let newchartdata = [];
          if (data != undefined)
          {
            if (data != "[]")
            {
              let dataxml = "<table>"+data+"</table>";

              this.promisesParser(dataxml).then(function(r){
                let tabledata = r['table'];
                let jsondata = tabledata['row'];
                let jsonstr = JSON.stringify(jsondata);
                jsonstr = jsonstr.split('[').join("");
                jsonstr = jsonstr.split(']').join("");
                
                try {
                  jsonstr = jsonstr.split('.00000000').join("");
                  jsonstr = jsonstr.split(todatestr).join("<strong>"+todatestr+"</strong>");
                } catch (error) {
                }
    
                let json=JSON.parse("["+jsonstr+"]");
                that.data= json;
                that.data = [...that.data]
                that.length = that.data.length;
                that.onChangeTable(that.config);
                that.rows=[...that.rows];
                mutex2.release();
              }).catch(function(e){
                mutex2.release();
                throw new Error(e) 
              });
            }
            else
            {
              let dataxml = "<table>"+data+"</table>";

              this.promisesParser(dataxml).then(function(r){
                let tabledata = r['table'];
                let jsondata = tabledata;
                let jsonstr = JSON.stringify(jsondata);
                jsonstr = jsonstr.split('[').join("");
                jsonstr = jsonstr.split(']').join("");
                let json=JSON.parse("["+jsonstr+"]");
                
                that.data=json;
                that.length = that.data.length;
                mutex2.release();
              }).catch(function(e){
                mutex2.release();
                throw new Error(e) 
              });
            }
          }
          else
          {
              let dataxml = "<table>[]</table>";

              this.promisesParser(dataxml).then(function(r){
                let tabledata = r['table'];
                let jsondata = tabledata;
                let jsonstr = JSON.stringify(jsondata);
                jsonstr = jsonstr.split('[').join("");
                jsonstr = jsonstr.split(']').join("");
                let json=JSON.parse("["+jsonstr+"]");
                
                that.data=json;
                that.length = that.data.length;
                mutex2.release();
              }).catch(function(e){
                mutex2.release();
                throw new Error(e) 
              });
          }

        
    })
    .catch(err => {
      mutex2.release();
      throw new Error(err) 
    });
  }

  public changePage(page:any, data:Array<any> = this.data):Array<any> {
    this.numPages = (page.itemsPerPage) ? Math.ceil(data.length/page.itemsPerPage) : this.numPages;
    page = (page.page) ? page.page : page;
    let start = (page - 1) * this.itemsPerPage;
    let end = this.itemsPerPage > -1 ? (start + this.itemsPerPage) : data.length;
    return data.slice(start, end);
  }

  public changeSort(data:any, config:any):any {
    if (!config.sorting) {
      return data;
    }

    let columns = this.config.sorting.columns || [];
    let columnName:string = void 0;
    let sort:string = void 0;

    for (let i = 0; i < columns.length; i++) {
      if (columns[i].sort !== '' && columns[i].sort !== false) {
        columnName = columns[i].name;
        sort = columns[i].sort;
      }
    }

    if (!columnName) {
      return data;
    }

    // simple sorting
    return data.sort((previous:any, current:any) => {
      if (previous[columnName] > current[columnName]) {
        return sort === 'desc' ? -1 : 1;
      } else if (previous[columnName] < current[columnName]) {
        return sort === 'asc' ? -1 : 1;
      }
      return 0;
    });
  }

  public changeFilter(data:any, config:any):any {
    let filteredData:Array<any> = data;
    this.columns.forEach((column:any) => {
      if (column.filtering) {
        filteredData = filteredData.filter((item:any) => {
          return item[column.name].match(column.filtering.filterString);
        });
      }
    });

    if (!config.filtering) {
      return filteredData;
    }

    if (config.filtering.columnName) {
      return filteredData.filter((item:any) =>
        item[config.filtering.columnName].match(this.config.filtering.filterString));
    }

    let tempArray:Array<any> = [];
    filteredData.forEach((item:any) => {
      let flag = false;
      this.columns.forEach((column:any) => {
        if (item[column.name].toString().match(this.config.filtering.filterString)) {
          flag = true;
        }
      });
      if (flag) {
        tempArray.push(item);
      }
    });
    filteredData = tempArray;

    return filteredData;
  }

  public onChangeTable(config:any, page:any = {page: this.page, itemsPerPage: this.itemsPerPage}):any {
    if (config.filtering) {
      Object.assign(this.config.filtering, config.filtering);
    }

    if (config.sorting) {
      Object.assign(this.config.sorting, config.sorting);
    }

    let filteredData = this.changeFilter(this.data, this.config);
    let sortedData = this.changeSort(filteredData, this.config);
    this.rows = page && config.paging ? this.changePage(page, sortedData) : sortedData;
    this.length = sortedData.length;
  }

  public onCellClick(data: any): any {
    //console.log(data);
  }

  urls: any = {
    DB: 'DB',
    jobs: 'jobs',
    sales: 'sales',
    coitems: 'coitems',
    workcenters: 'workcenters',
    workcenter: 'workcenter',
  };

  getDB(): Promise<any> {

    return this.http.get(this.baseURL+"/"+this.urls.DB+"/", { responseType: 'text' })//(this.baseURL+"/"+this.urls.DB+"/")
    .pipe(
      map(this.extractData),
      catchError(this.handleError)
    )
    .toPromise();
  }

  getAllJobs(): Promise<any> {
    
    return this.http.get(this.baseURL+"/"+this.urls.jobs, { responseType: 'text' })//(this.baseURL+"/"+this.urls.jobs)
    .pipe(
      map(this.extractData),
      catchError(this.handleError)
    )
    .toPromise();
  }

  getAllSales(): Promise<any> {
    
    return this.http.get(this.baseURL+"/"+this.urls.sales, { responseType: 'text' })
    .pipe(
      map(this.extractData),
      catchError(this.handleError)
    )
    .toPromise();
  }

  getCoitems(from: string, to: string): Promise<any> {

    let fromdate = new Date(from);
    let todate = new Date(to);
    
    let fromdatestr = fromdate.getFullYear()+"-"+(fromdate.getMonth()+1)+"-"+fromdate.getDate();
    let todatestr = todate.getFullYear()+"-"+(todate.getMonth()+1)+"-"+todate.getDate();
    
   return this.http.get(this.baseURL+"/"+this.urls.coitems+"/"+fromdatestr+"/"+todatestr, { responseType: 'text' })//(this.baseURL+"/"+this.urls.coitems+"/"+fromdate+"/"+todatestr)
    .pipe(
        map(this.extractData),
        catchError(this.handleError)
      )
      .toPromise();
  }
  
  getListOfWorkcenters(): Promise<any> {
    
    return this.http.get(this.baseURL+"/"+this.urls.workcenters+"/", { responseType: 'text' })//(this.baseURL+"/"+this.urls.workcenters+"/").pipe(
      .pipe(
        map(this.extractData),
        catchError(this.handleError)
      )
      .toPromise();
  }

  getAllWorkcenter(from: string, to: string,workcenter:string): Promise<any> {

    let fromdate = new Date(from);
    let todate = new Date(to);
    
    //let datearray = [];
    let getarray = [];
    let count = 0;
    for (var date = fromdate; date <= todate; date.setDate(date.getDate() + 1)) {
        //datearray.push(new Date(date));
        let datestr = date.getFullYear()+"-"+(date.getMonth()+1)+"-"+date.getDate();
        let req = this.http.get(this.baseURL+"/"+this.urls.workcenters+"/"+datestr, { responseType: 'text' });


         req = this.http.get(this.baseURL+"/"+this.urls.workcenter+"/"+datestr+"/"+workcenter, { responseType: 'text' });
        
    
        count++;

        getarray.push(req);
    }

    let date7  = new Date(date);
    let date6 = new Date(date);
    date6.setDate(date7.getDate()-1);
    let date5 = new Date(date);
    date5.setDate(date6.getDate()-1);
    let date4 = new Date(date);
    date4.setDate(date5.getDate()-1);
    let date3= new Date(date);
    date3.setDate(date4.getDate()-1);
    let date2 = new Date(date);
    date2.setDate(date3.getDate()-1);
    let date1 = new Date(date);
    date1.setDate(date2.getDate()-1);


      let datestr7 = date7.getFullYear()+"-"+(date7.getMonth()+1)+"-"+date7.getDate();
      let datestr6 = date6.getFullYear()+"-"+(date6.getMonth()+1)+"-"+date6.getDate();
      let datestr5 = date5.getFullYear()+"-"+(date5.getMonth()+1)+"-"+date5.getDate();
      let datestr4 = date4.getFullYear()+"-"+(date4.getMonth()+1)+"-"+date4.getDate();
      let datestr3 = date3.getFullYear()+"-"+(date3.getMonth()+1)+"-"+date3.getDate();
      let datestr2 = date2.getFullYear()+"-"+(date2.getMonth()+1)+"-"+date2.getDate();
      let datestr1 = date1.getFullYear()+"-"+(date1.getMonth()+1)+"-"+date1.getDate();
      let day7 = this.http.get(this.baseURL+"/"+this.urls.workcenter+"/"+datestr7, { responseType: 'text' });//(this.baseURL+"/"+this.urls.workcenters+"/"+date7)
      let day6 = this.http.get(this.baseURL+"/"+this.urls.workcenter+"/"+datestr6, { responseType: 'text' });//(this.baseURL+"/"+this.urls.workcenters+"/"+date6)
      let day5 = this.http.get(this.baseURL+"/"+this.urls.workcenter+"/"+datestr5, { responseType: 'text' });//(this.baseURL+"/"+this.urls.workcenters+"/"+date5)
      let day4 = this.http.get(this.baseURL+"/"+this.urls.workcenter+"/"+datestr4, { responseType: 'text' });//(this.baseURL+"/"+this.urls.workcentes+"/"+date4)
      let day3 = this.http.get(this.baseURL+"/"+this.urls.workcenter+"/"+datestr3, { responseType: 'text' });//(this.baseURL+"/"+this.urls.workcenters+"/"+date3)
      let day2 = this.http.get(this.baseURL+"/"+this.urls.workcenter+"/"+datestr2, { responseType: 'text' });//(this.baseURL+"/"+this.urls.workcenters+"/"+date2)
      let day1 = this.http.get(this.baseURL+"/"+this.urls.workcenter+"/"+datestr1, { responseType: 'text' });//(this.baseURL+"/"+this.urls.workcenters+"/"+date1)

  
    
    return forkJoin([day1, day2, day3,, day4, day5, day6, day7])
    .pipe(
      map(this.extractData),
      catchError(this.handleError)
    )
    .toPromise();

  
  }

  getWorkcenter(from: string, to: string, workcenter: string): Promise<any> {

    let fromdate = new Date(from);
    let todate = new Date(to);
    
    let fromdatestr = fromdate.getFullYear()+"-"+(fromdate.getMonth()+1)+"-"+fromdate.getDate();
    let todatestr = todate.getFullYear()+"-"+(todate.getMonth()+1)+"-"+todate.getDate();
    
    if (workcenter=="")
    {
   return this.http.get(this.baseURL+"/"+this.urls.workcenters+"/"+fromdatestr+"/"+todatestr, { responseType: 'text' })//(this.baseURL+"/"+this.urls.workcenters+"/"+fromdate+"/"+todatestr)
    .pipe(
        map(this.extractData),
        catchError(this.handleError)
      )
      .toPromise();
    }
    else
    {
      return this.http.get(this.baseURL+"/"+this.urls.workcenter+"/"+fromdatestr+"/"+todatestr+"/"+workcenter, { responseType: 'text' })//(this.baseURL+"/"+this.urls.workcenter+"/"+fromdate+"/"+todatestr)
      .pipe(
          map(this.extractData),
          catchError(this.handleError)
        )
        .toPromise();
      }
  }

  getAllJobsAchievement(from: string, to: string): Promise<any> {

    let fromdate = new Date(from);
    let todate = new Date(to);
    
    let fromdatestr = fromdate.getFullYear()+"-"+(fromdate.getMonth()+1)+"-"+fromdate.getDate();
    let todatestr = todate.getFullYear()+"-"+(todate.getMonth()+1)+"-"+todate.getDate();
    
     return this.http.get(this.baseURL+"/"+this.urls.jobs+"/"+fromdatestr+"/"+todatestr, { responseType: 'text' })
    .pipe(
        map(this.extractData),
        catchError(this.handleError)
      )
      .toPromise();
  }

  private extractData(res: Response | any) {
    return res;
  }

  private handleError(err: Response | any) {
    if (!err.Message) {
      err.Message = err.statusText;
    }
    return Observable.throw(err.Message || err);
  }

}
