import { Component, Injectable, OnInit } from '@angular/core';
import { NgbDateAdapter, NgbDateStruct,NgbDateParserFormatter} from '@ng-bootstrap/ng-bootstrap';
import { TableData } from './data';
import {single, multi} from './linechartdata'
import * as global from '../../../config/globals';
import { Observable } from 'rxjs/Rx';
import { DatePipe } from '@angular/common';
import { IDropdownSettings } from 'ng-multiselect-dropdown';
import moment from 'moment';
import {Mutex, MutexInterface, Semaphore, SemaphoreInterface, withTimeout} from 'async-mutex';
//import {Parser} from 'xml2js';
const mutex1 = new Mutex();
const mutex2 = new Mutex();
import { HttpClient } from '@angular/common/http';
// { Http, Response,Headers, RequestOptions} from '@angular/http';
import {forkJoin } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { isUndefined } from 'util';
import { Result } from '../../../models/result'
import {Parser} from 'xml2js';
// for ngb datepicker adapter
@Injectable()
export class CustomDateParserFormatter extends NgbDateParserFormatter {

  readonly DELIMITER = '/';

  parse(value: string): NgbDateStruct | null {
    if (value) {
      let date = value.split(this.DELIMITER);
      return {
        day : parseInt(date[0], 10),
        month : parseInt(date[1], 10),
        year : parseInt(date[2], 10)
      };
    }
    return null;
  }

  format(date: NgbDateStruct | null): string {
    return date ? date.day + this.DELIMITER + date.month + this.DELIMITER + date.year : '';
  }
}

@Component({
  selector: 'chart-ngx',
  templateUrl: './chart-ngx.html',
  providers: [{provide: NgbDateParserFormatter, useClass: CustomDateParserFormatter}]
})

export class ChartNgxPage implements OnInit {
  

  global = global;
  isDB = true;
  isLive = true;
  tdelay = 5000;
  // ngbdatepicker
  modelDate1: any;
  modelDate2: any;
  get today() {
    return new Date();
  }

  workcenterlist=[];
  dropdownSettings = {
    singleSelection: true,
    idField: 'name',
    textField: 'name',
    closeDropDownOnSelection: true,
    itemsShowLimit: 1,
    allowSearchFilter: true
  };
  
  selectedworkcenterlist=[];
  workcenter:string;
  

  //stackedBarChart
  stackedBarChartData=[];

  private data:Array<any> = TableData;

  //linechartData
  lineChartData=[];

  
  chartData;
  chartColor;
  areaChartData;
  areaChartColor;
  pieChartData;
  barChartData;
  heatMapChartData;

  // line chart
  single: any[];
  multi: any[];

  //view: any[] = [400, 400];

  // options
  showXAxis = true;
  showYAxis = true;
  gradient = false;
  showLegend = true;
  showXAxisLabel = true;
  xAxisLabel = 'Workcenter';
  showYAxisLabel = true;
  yAxisLabel = 'Number';
  showRefLines= true;
  showRefLabels = true;
  referenceLines = [
    {
      name: "Max",
      value: "7500000",
      //
      //style: {
      //  marker: {
      //    fill: "#A4A7D5",
      //    stroke: "#A4A7D5"
      //  }
      //}
    },
    {
      name: "Min",
      value: "6500000"
      //,
      //style: {
      // marker: {
      //    fill: "#87DEDB",
      //    stroke: "#87DEDB"
      //  }
      //}
    }
  ]

  colorScheme = {
    domain: ['#5AA454', '#A10A28', '#C7B42C', '#AAAAAA']
  };

  // line, area
  autoScale = true;

  onSelect(event) {
    //console.log(event);
  }
   // line chart

  //FromDate change//
  onDate1Selected(item: any)
  {
    //console.log(item);
     mutex1.runExclusive(async () => {
      try {
          this.LoadCoItem();
      } finally {
          // Please read and understand the WARNING above before using this API.
      }
    });

     mutex2.runExclusive(async () => {
      try {
          this.LoadJob();
      } finally {
          // Please read and understand the WARNING above before using this API.
      }
    });
    
  }
  //ToDate change//
  onDate2Selected(item: any)
  {
    //console.log(item);
    mutex1.runExclusive(async () => {
      try {
          this.LoadCoItem();
      } finally {
          // Please read and understand the WARNING above before using this API.
      }
    });

     mutex2.runExclusive(async () => {
      try {
          this.LoadJob();
      } finally {
          // Please read and understand the WARNING above before using this API.
      }
    });
  }
  //Dropdown box change//
  onItemSelect(item: any) {
    //console.log(item);
    this.workcenter=item.name;
    mutex1.runExclusive(async () => {
      try {
          this.LoadCoItem();
      } finally {
          // Please read and understand the WARNING above before using this API.
      }
    });

     mutex2.runExclusive(async () => {
      try {
          this.LoadJob();
      } finally {
          // Please read and understand the WARNING above before using this API.
      }
    });
    //console.log(this.workcenter);
  }

  onItemDeSelect(item: any) {
    this.workcenter="";
    //console.log(item);
    mutex1.runExclusive(async () => {
      try {
          this.LoadCoItem();
      } finally {
          // Please read and understand the WARNING above before using this API.
      }
    });

     mutex2.runExclusive(async () => {
      try {
          this.LoadJob();
      } finally {
          // Please read and understand the WARNING above before using this API.
      }
    });
    //console.log(this.workcenter);
  }


  onSelectAll(items: any) {
    //console.log(items);
    /*
    this.selectedworkcenterlist.forEach(element => {
      if (this.workcenter=="")
      {
        this.workcenter= "'"+element.name+"'";
      }
      else
      {
        this.workcenter= this.workcenter + "," + "'"+element.name+"'";
      }
    });
    */
    //console.log(this.workcenter);
  }

  ngOnInit() {

    let dateParts = this.datepipe.transform(this.today, 'yyyy-MM-dd').split('-');
    this.modelDate1 = { year: parseInt(dateParts[0],10), month: parseInt(dateParts[1],10), day: parseInt(dateParts[2],10) };
    this.modelDate2 = { year: parseInt(dateParts[0],10), month: parseInt(dateParts[1],10), day: parseInt(dateParts[2],10) };
    this.onChangeTable(this.config);
    this.chartColor = { domain: [global.COLOR_BLUE, global.COLOR_GREEN, global.COLOR_PURPLE, global.COLOR_BLACK] };
    
    //workcenter // count of workcenter //To Date 7 day before
    //this.stackedBarChartData = [{name:"Germany",series:[{name:"2010",value:40632},{name:"2000",value:36953},{name:"1990",value:31476}]},{name:"United States",series:[{name:"2010",value:49737},{name:"2000",value:45986},{name:"1990",value:37060}]},{name:"France",series:[{name:"2010",value:36745},{name:"2000",value:34774},{name:"1990",value:29476}]},{name:"United Kingdom",series:[{name:"2010",value:36240},{name:"2000",value:32543},{name:"1990",value:26424}]}];
    //workcenter // count of workcenter //From date - To Date
    //this.pieChartData = [{"name":"Germany","value":8940000},{"name":"USA","value":5000000},{"name":"France","value":7200000}];
    //basic line chart x=workcenter y=count of workcenter //From date - To Date

  }
  
  public rows:Array<any> = [];
  public columns:Array<any> = [
    {
      title: 'Co Num', name: 'co_num', sort: '', 
      //filtering: {filterString: '', placeholder: 'Filter by name'}
    },
    {
      title: 'Due Date Day', name: 'due_date_day', sort: '', 
      //filtering: {filterString: '', placeholder: 'Filter by extn.'}
    }
  ];

  public page:number = 1;
  public itemsPerPage:number = 5;
  public maxSize:number = 4;
  public numPages:number = 1;
  public length:number = 0;

  public config:any = {
    paging: true,
    sorting: {columns: this.columns},
    //filtering: {filterString: ''},
    className: ['table-striped', 'table-bordered', 'm-b-0']
  };

  baseURL:string;
  public constructor(public datepipe: DatePipe, private http:HttpClient) { 
    this.baseURL= "http://localhost:8008";
    
    Object.assign(this, {single, multi});
    this.length = this.data.length;
    let dateParts = this.datepipe.transform(this.today, 'yyyy-MM-dd').split('-');
    this.modelDate1 = { year: parseInt(dateParts[0],10), month: parseInt(dateParts[1],10), day: parseInt(dateParts[2],10) };
    this.modelDate2 = { year: parseInt(dateParts[0],10), month: parseInt(dateParts[1],10), day: parseInt(dateParts[2],10) };
    this.LoadWorkcenter();

    mutex1.runExclusive(async () => {
      try {
          this.LoadCoItem();
      } finally {
          // Please read and understand the WARNING above before using this API.
      }
    });

     mutex2.runExclusive(async () => {
      try {
          this.LoadJob();
      } finally {
          // Please read and understand the WARNING above before using this API.
      }
    });

    Observable.interval(this.tdelay).subscribe(x => { // will execute every 5 seconds
      if (this.isLive)
      {
        if (this.isDB)
        {
          let that = this;
          this.getDB()
          .then(data => {
            if (data=="1")
            {
              mutex1.runExclusive(async () => {
                try {
                    that.LoadCoItem();
                } finally {
                    // Please read and understand the WARNING above before using this API.
                }
              });
          
               mutex2.runExclusive(async () => {
                try {
                    that.LoadJob();
                } finally {
                    // Please read and understand the WARNING above before using this API.
                }
              });
            }
          });
        }
        else
        {

          mutex1.runExclusive(async () => {
            try {
                this.LoadCoItem();
            } finally {
                // Please read and understand the WARNING above before using this API.
            }
          });
      
           mutex2.runExclusive(async () => {
            try {
                this.LoadJob();
            } finally {
                // Please read and understand the WARNING above before using this API.
            }
          });
          
        }
      }
    });
  }

  private promisesParser(string)
  {
      var xml2jsParser = require('xml2js').parseString
      return new Promise(function(resolve, reject)
      {
        xml2jsParser(string, function(err, result) {
              if (err) {
                  return reject(err);
               } else {
                  return resolve(result);
               }
          });
      });
  }

  private convert(n) {
    n = String(n)
    if (n.length == 1)
      n = '0' + n
    return n
  }

  private LoadCoItem()
  {
    let fromdatemodel = this.modelDate1;
    let fromdatestr = fromdatemodel['year']+"-"+this.convert(fromdatemodel['month'])+"-"+this.convert(fromdatemodel['day']);
    let todatemodel = this.modelDate2;
    let todatestr = todatemodel['year']+"-"+this.convert(todatemodel['month'])+"-"+this.convert(todatemodel['day']);
    
    let fromdate = new Date(fromdatestr);
    let todate = new Date(todatestr);
    let tablefixdiffInDays = 2;
    let tabletodate = new Date(todatestr);
    tabletodate.setDate(tabletodate.getDate()-tablefixdiffInDays);
    let tablefixfromdatestr = tabletodate.getFullYear()+"-"+this.convert(tabletodate.getMonth()+1)+"-"+this.convert(tabletodate.getDate());
    let tablefixfromdate = new Date(tablefixfromdatestr);
    
    this.getCoitems(tablefixfromdatestr,todatestr)
    .then(data => {
        let that = this;
        let newchartdata = [];
          if (data != undefined)
          {
            if (data != "[]")
            {
              let dataxml = "<table>"+data+"</table>";

              this.promisesParser(dataxml).then(function(r){
                let tabledata = r['table'];
                let jsondata = tabledata['row'];
                let jsonstr = JSON.stringify(jsondata);
                jsonstr = jsonstr.split('[').join("");
                jsonstr = jsonstr.split(']').join("");
                jsonstr = jsonstr.split('T00:00:00').join("");
                try {
                  jsonstr = jsonstr.split(todatestr).join("<strong>"+todatestr+"</strong>");
                } catch (error) {
                }
    
                let json=JSON.parse("["+jsonstr+"]");
                that.data= json;
                that.data = [...that.data]
                that.length = that.data.length;
                that.onChangeTable(that.config);
                that.rows=[...that.rows];
                mutex1.release();
   
              }).catch(function(e){
                mutex1.release();
                throw new Error(e) 
              });
            }
            else
            {
              let dataxml = "<table>"+data+"</table>";

              this.promisesParser(dataxml).then(function(r){
                let tabledata = r['table'];
                let jsondata = tabledata;
                let jsonstr = JSON.stringify(jsondata);
                jsonstr = jsonstr.split('[').join("");
                jsonstr = jsonstr.split(']').join("");
                let json=JSON.parse("["+jsonstr+"]");
                
                that.data=json;
                that.length = that.data.length;
                mutex1.release();
              }).catch(function(e){
                mutex1.release();
                throw new Error(e) 
              });
            }
          }
          else
          {
              let dataxml = "<table>[]</table>";

              this.promisesParser(dataxml).then(function(r){
                let tabledata = r['table'];
                let jsondata = tabledata;
                let jsonstr = JSON.stringify(jsondata);
                jsonstr = jsonstr.split('[').join("");
                jsonstr = jsonstr.split(']').join("");
                let json=JSON.parse("["+jsonstr+"]");
                
                that.data=json;
                that.length = that.data.length;
                mutex1.release();
              }).catch(function(e){
                mutex1.release();
                throw new Error(e) 
              });
          }

        
    })
    .catch(err => {
      mutex1.release();
      throw new Error(err)
      
    });
  }

  private LoadWorkcenter()
  {
    this.getListOfWorkcenters()
    .then(data => {
        let that = this;
        let newchartdata = [];
        let dataxml = "<table>"+data+"</table>";
        this.promisesParser(dataxml).then(function(r){
          let tabledata = r['table'];
          let jsondata = tabledata['row'];
          let jsonstr = JSON.stringify(jsondata);
          jsonstr = jsonstr.split('[').join("");
          jsonstr = jsonstr.split(']').join("");
          let json=JSON.parse("["+jsonstr+"]");
          
          that.workcenterlist=json;
          
        }).catch(function(e){
          throw new Error(e) 
        });
      })
      .catch(err => {
         throw new Error(err) 
      });
  }

  private LoadJob()
  {
    //let parser = require('xml2js').Parser();
    let fromdatemodel = this.modelDate1;
    let fromdatestr = fromdatemodel['year']+"-"+this.convert(fromdatemodel['month'])+"-"+this.convert(fromdatemodel['day']);
    let todatemodel = this.modelDate2;
    let todatestr = todatemodel['year']+"-"+this.convert(todatemodel['month'])+"-"+this.convert(todatemodel['day']);
    
    let fromdate = new Date(fromdatestr);
    let todate = new Date(todatestr);
    let firstDate = moment(fromdatestr);
    let secondDate = moment(todatestr);
    //let diffInDays = Math.abs(firstDate.diff(secondDate, 'days')); 

    let fixdiffInDays = 6;
    let t = new Date(todatestr);
    t.setDate(t.getDate()-fixdiffInDays);
    let fixfromdatestr = t.getFullYear()+"-"+this.convert(t.getMonth()+1)+"-"+this.convert(t.getDate());
    let fixfromdate = new Date(fixfromdatestr);
    
    let count = 0;
    
    
    let wc = "'"+this.workcenter+"'"
    if (this.workcenter=="" || this.workcenter == undefined)
    {
      wc="";
    }

    this.getAllWorkcenter(fixfromdatestr,todatestr,wc,true)
    .then(data => {
        let that = this;
        let newchartdata = [];

        let count = 0;
        for (var date = fixfromdate; date <= todate; date.setDate(date.getDate() + 1))
        {
          let weekday=date.getDay();

          if (weekday==0)
          {
            try {
              weekday = Number(String(weekday).split('0').join('7'));
            } catch (error) {
            }
          }
          
          if (data[count] != undefined)
          {
            if (data[count] != "[]")
            {
              let dataxml = "<table>"+data[count]+"</table>";

              this.promisesParser(dataxml).then(function(r){
                let tabledata = r['table'];
                let jsondata = tabledata['row'];
                let jsonstr = JSON.stringify(jsondata);
                jsonstr = jsonstr.split('[').join("");
                jsonstr = jsonstr.split(']').join("");
                jsonstr = jsonstr.split('"value":"').join('"value":');
                jsonstr = jsonstr.split('"}').join("}");
                let json=JSON.parse("["+jsonstr+"]");
                
                newchartdata.push({name:weekday,series:json});
                that.stackedBarChartData=[...newchartdata];
                if (count==fixdiffInDays)
                {
                  mutex2.release();
                }
              }).catch(function(e){
                mutex2.release();
                throw new Error(e) 
              });
            }
            else
            {
              let dataxml = "<table>"+data[count]+"</table>";

              this.promisesParser(dataxml).then(function(r){
                let tabledata = r['table'];
                let jsondata = tabledata;
                let jsonstr = JSON.stringify(jsondata);
                jsonstr = jsonstr.split('[').join("");
                jsonstr = jsonstr.split(']').join("");
                let json=JSON.parse("["+jsonstr+"]");
                
                newchartdata.push({name:weekday,series:[]});
                that.stackedBarChartData=[...newchartdata];
                if (count==fixdiffInDays)
                {
                  mutex2.release();
                }
              }).catch(function(e){
                mutex2.release();
                throw new Error(e) 
              });
            }
          }
          else
          {
              let dataxml = "<table>[]</table>";

              this.promisesParser(dataxml).then(function(r){
                let tabledata = r['table'];
                let jsondata = tabledata;
                let jsonstr = JSON.stringify(jsondata);
                jsonstr = jsonstr.split('[').join("");
                jsonstr = jsonstr.split(']').join("");
                let json=JSON.parse("["+jsonstr+"]");
                
                newchartdata.push({name:weekday,series:[]});
                that.stackedBarChartData=[...newchartdata];
                if (count==fixdiffInDays)
                {
                  mutex2.release();
                }
              }).catch(function(e){
                mutex2.release();
                throw new Error(e) 
              });
          }
          count++;
        }

      /*
        let dataxml = "<table>"+data[6]+"</table>";
        this.promisesParser(dataxml).then(function(r){
          let tabledata = r['table'];
          let jsondata = tabledata['row'];
          let jsonstr = JSON.stringify(jsondata);
          jsonstr = jsonstr.split('[').join("");
          jsonstr = jsonstr.split(']').join("");
          jsonstr = jsonstr.split('"value":"').join('"value":');
          jsonstr = jsonstr.split('"}').join("}");
          let json=JSON.parse("["+jsonstr+"]");
          
          newchartdata.push({name:weekday1,series:json});
          that.stackedBarChartData=[...newchartdata];
          
        }).catch(function(e){
          throw new Error(e) 
        });

        dataxml = "<table>"+data[5]+"</table>";
        this.promisesParser(dataxml).then(function(r){
          let tabledata = r['table'];
          let jsondata = tabledata['row'];
          let jsonstr = JSON.stringify(jsondata);
          jsonstr = jsonstr.split('[').join("");
          jsonstr = jsonstr.split(']').join("");
          jsonstr = jsonstr.split('"value":"').join('"value":');
          jsonstr = jsonstr.split('"}').join("}");
          let json=JSON.parse("["+jsonstr+"]");
          
          newchartdata.push({name:weekday2,series:json});
          that.stackedBarChartData=[...newchartdata];
          
        }).catch(function(e){
          throw new Error(e) 
        });

        dataxml = "<table>"+data[4]+"</table>";
        this.promisesParser(dataxml).then(function(r){
          let tabledata = r['table'];
          let jsondata = tabledata['row'];
          let jsonstr = JSON.stringify(jsondata);
          jsonstr = jsonstr.split('[').join("");
          jsonstr = jsonstr.split(']').join("");
          jsonstr = jsonstr.split('"value":"').join('"value":');
          jsonstr = jsonstr.split('"}').join("}");
          let json=JSON.parse("["+jsonstr+"]");
          
          newchartdata.push({name:weekday3,series:json});
          that.stackedBarChartData=[...newchartdata];
          
        }).catch(function(e){
          throw new Error(e) 
        });

        dataxml = "<table>"+data[3]+"</table>";
        this.promisesParser(dataxml).then(function(r){
          let tabledata = r['table'];
          let jsondata = tabledata['row'];
          let jsonstr = JSON.stringify(jsondata);
          jsonstr = jsonstr.split('[').join("");
          jsonstr = jsonstr.split(']').join("");
          jsonstr = jsonstr.split('"value":"').join('"value":');
          jsonstr = jsonstr.split('"}').join("}");
          let json=JSON.parse("["+jsonstr+"]");
          
          newchartdata.push({name:weekday4,series:json});
          that.stackedBarChartData=[...newchartdata];
          
        }).catch(function(e){
          throw new Error(e) 
        });

        dataxml = "<table>"+data[2]+"</table>";
        this.promisesParser(dataxml).then(function(r){
          let tabledata = r['table'];
          let jsondata = tabledata['row'];
          let jsonstr = JSON.stringify(jsondata);
          jsonstr = jsonstr.split('[').join("");
          jsonstr = jsonstr.split(']').join("");
          jsonstr = jsonstr.split('"value":"').join('"value":');
          jsonstr = jsonstr.split('"}').join("}");
          let json=JSON.parse("["+jsonstr+"]");
          
          newchartdata.push({name:weekday5,series:json});
          that.stackedBarChartData=[...newchartdata];
          
        }).catch(function(e){
          throw new Error(e) 
        });

        dataxml = "<table>"+data[1]+"</table>";
        this.promisesParser(dataxml).then(function(r){
          let tabledata = r['table'];
          let jsondata = tabledata['row'];
          let jsonstr = JSON.stringify(jsondata);
          jsonstr = jsonstr.split('[').join("");
          jsonstr = jsonstr.split(']').join("");
          jsonstr = jsonstr.split('"value":"').join('"value":');
          jsonstr = jsonstr.split('"}').join("}");
          let json=JSON.parse("["+jsonstr+"]");
          
          newchartdata.push({name:weekday6,series:json});
          that.stackedBarChartData=[...newchartdata];
          
        }).catch(function(e){
          throw new Error(e) 
        });

        dataxml = "<table>"+data[0]+"</table>";
        this.promisesParser(dataxml).then(function(r){
          let tabledata = r['table'];
          let jsondata = tabledata['row'];
          let jsonstr = JSON.stringify(jsondata);
          jsonstr = jsonstr.split('[').join("");
          jsonstr = jsonstr.split(']').join("");
          jsonstr = jsonstr.split('"value":"').join('"value":');
          jsonstr = jsonstr.split('"}').join("}");
          let json=JSON.parse("["+jsonstr+"]");
          
          newchartdata.push({name:weekday7,series:json});
          that.stackedBarChartData=[...newchartdata];
          
        }).catch(function(e){
          throw new Error(e) 
        });

      */
        
    })
    .catch(err => {
      mutex2.release();
      throw new Error(err) 
    });
    
    if (wc!="")
    {

    this.getAllWorkcenter(fromdatestr,todatestr,wc,false)
    .then(data => {
        //console.log(jobdata);
        let that = this;
        let newchartdata = [];

        let count = 0;
        
          let weekday=wc;
          try {
            weekday = weekday.split("'").join("");
          } catch (error) {
          }
          if (data[count] != undefined)
          {
            if (data[count] != "[]")
            {
              let dataxml = "<table>"+data[count]+"</table>";
              this.promisesParser(dataxml).then(function(r){
                let tabledata = r['table'];
                let jsondata = tabledata['row'];
                let jsonstr = JSON.stringify(jsondata);
                jsonstr = jsonstr.split('[').join("");
                jsonstr = jsonstr.split(']').join("");
                jsonstr = jsonstr.split('"value":"').join('"value":');
                jsonstr = jsonstr.split('"}').join("}");
                let json=JSON.parse("["+jsonstr+"]");
                
                newchartdata.push({name:weekday,series:json});
                that.lineChartData=[...newchartdata];
                
              }).catch(function(e){
                throw new Error(e) 
              });
            }
            else
            {
              let dataxml = "<table>"+data[count]+"</table>";

              this.promisesParser(dataxml).then(function(r){
                let tabledata = r['table'];
                let jsondata = tabledata;
                let jsonstr = JSON.stringify(jsondata);
                jsonstr = jsonstr.split('[').join("");
                jsonstr = jsonstr.split(']').join("");
                let json=JSON.parse("["+jsonstr+"]");
                
                newchartdata.push({name:weekday,series:[]});
                that.lineChartData=[...newchartdata];
              }).catch(function(e){
                throw new Error(e) 
              });
            }
          }
          else
          {
              let dataxml = "<table>[]</table>";

              this.promisesParser(dataxml).then(function(r){
                let tabledata = r['table'];
                let jsondata = tabledata;
                let jsonstr = JSON.stringify(jsondata);
                jsonstr = jsonstr.split('[').join("");
                jsonstr = jsonstr.split(']').join("");
                let json=JSON.parse("["+jsonstr+"]");
                
                newchartdata.push({name:weekday,series:[]});
                that.lineChartData=[...newchartdata];
              }).catch(function(e){
                throw new Error(e) 
              });
          }
          
    })
    .catch(err => {
        throw new Error(err) 
    });

    }
    else
    {
      let newchartdata = [];
      this.lineChartData=[...newchartdata];
    }

    this.getWorkcenter(fromdatestr,todatestr,"")
    .then(data => {
        let that = this;
        let newchartdata = [];
        let dataxml = "<table>"+data+"</table>";
        this.promisesParser(dataxml).then(function(r){
          let tabledata = r['table'];
          let jsondata = tabledata['row'];
          let jsonstr = JSON.stringify(jsondata);
          jsonstr = jsonstr.split('[').join("");
          jsonstr = jsonstr.split(']').join("");
          jsonstr = jsonstr.split('"value":"').join('"value":');
          jsonstr = jsonstr.split('"}').join("}");
          let json=JSON.parse("["+jsonstr+"]");
          
          that.pieChartData=json;
          
        }).catch(function(e){
          throw new Error(e) 
        });
    })
    .catch(err => {
        throw new Error(err) 
    });
  }
  
  public changePage(page:any, data:Array<any> = this.data):Array<any> {
    this.numPages = (page.itemsPerPage) ? Math.ceil(data.length/page.itemsPerPage) : this.numPages;
    page = (page.page) ? page.page : page;
    let start = (page - 1) * this.itemsPerPage;
    let end = this.itemsPerPage > -1 ? (start + this.itemsPerPage) : data.length;
    return data.slice(start, end);
  }

  public changeSort(data:any, config:any):any {
    if (!config.sorting) {
      return data;
    }

    let columns = this.config.sorting.columns || [];
    let columnName:string = void 0;
    let sort:string = void 0;

    for (let i = 0; i < columns.length; i++) {
      if (columns[i].sort !== '' && columns[i].sort !== false) {
        columnName = columns[i].name;
        sort = columns[i].sort;
      }
    }

    if (!columnName) {
      return data;
    }

    // simple sorting
    return data.sort((previous:any, current:any) => {
      if (previous[columnName] > current[columnName]) {
        return sort === 'desc' ? -1 : 1;
      } else if (previous[columnName] < current[columnName]) {
        return sort === 'asc' ? -1 : 1;
      }
      return 0;
    });
  }

  public changeFilter(data:any, config:any):any {
    let filteredData:Array<any> = data;
    this.columns.forEach((column:any) => {
      if (column.filtering) {
        filteredData = filteredData.filter((item:any) => {
          return item[column.name].match(column.filtering.filterString);
        });
      }
    });

    if (!config.filtering) {
      return filteredData;
    }

    if (config.filtering.columnName) {
      return filteredData.filter((item:any) =>
        item[config.filtering.columnName].match(this.config.filtering.filterString));
    }

    let tempArray:Array<any> = [];
    filteredData.forEach((item:any) => {
      let flag = false;
      this.columns.forEach((column:any) => {
        if (item[column.name].toString().match(this.config.filtering.filterString)) {
          flag = true;
        }
      });
      if (flag) {
        tempArray.push(item);
      }
    });
    filteredData = tempArray;

    return filteredData;
  }

  public onChangeTable(config:any, page:any = {page: this.page, itemsPerPage: this.itemsPerPage}):any {
    if (config.filtering) {
      Object.assign(this.config.filtering, config.filtering);
    }

    if (config.sorting) {
      Object.assign(this.config.sorting, config.sorting);
    }

    let filteredData = this.changeFilter(this.data, this.config);
    let sortedData = this.changeSort(filteredData, this.config);
    this.rows = page && config.paging ? this.changePage(page, sortedData) : sortedData;
    this.length = sortedData.length;
  }

  public onCellClick(data: any): any {
    //console.log(data);
  }

  urls: any = {
    DB: 'DB',
    jobs: 'jobs',
    sales: 'sales',
    coitems: 'coitems',
    workcenters: 'workcenters',
    workcenter: 'workcenter',
    line: 'line'
  };

  getDB(): Promise<any> {
    return this.http.get(this.baseURL+"/"+this.urls.DB+"/", { responseType: 'text' })
    .pipe(
      map(this.extractData),
      catchError(this.handleError)
    )
    .toPromise();
  }

  getAllJobs(): Promise<any> {
    
    return this.http.get(this.baseURL+"/"+this.urls.jobs, { responseType: 'text' })//(this.baseURL+"/"+this.urls.jobs)
    .pipe(
      map(this.extractData),
      catchError(this.handleError)
    )
    .toPromise();
  }

  getAllSales(): Promise<any> {
    
    return this.http.get(this.baseURL+"/"+this.urls.sales, { responseType: 'text' })
    .pipe(
      map(this.extractData),
      catchError(this.handleError)
    )
    .toPromise();
  }

  getCoitems(from: string, to: string): Promise<any> {

    let fromdate = new Date(from);
    let todate = new Date(to);
    
    let fromdatestr = fromdate.getFullYear()+"-"+(fromdate.getMonth()+1)+"-"+fromdate.getDate();
    let todatestr = todate.getFullYear()+"-"+(todate.getMonth()+1)+"-"+todate.getDate();
    
   return this.http.get(this.baseURL+"/"+this.urls.coitems+"/"+fromdatestr+"/"+todatestr, { responseType: 'text' })//(this.baseURL+"/"+this.urls.coitems+"/"+fromdate+"/"+todatestr)
    .pipe(
        map(this.extractData),
        catchError(this.handleError)
      )
      .toPromise();
  }
  
  getListOfWorkcenters(): Promise<any> {
    return this.http.get(this.baseURL+"/"+this.urls.workcenters+"/", { responseType: 'text' })
      .pipe(
        map(this.extractData),
        catchError(this.handleError)
      )
      .toPromise();
  }

  getAllWorkcenter(from: string, to: string, workcenter:string, flag:boolean): Promise<any> {

    let fromdate = new Date(from);
    let todate = new Date(to);
    
    //let datearray = [];
    let getarray = [];
    let count = 0;

    if (!flag)
    {
 
        let datestr = todate.getFullYear()+"-"+(todate.getMonth()+1)+"-"+todate.getDate();
        if (workcenter!="")
        {
          let req = this.http.get(this.baseURL+"/"+this.urls.line+"/"+datestr+"/"+ workcenter, { responseType: 'text' });
          getarray.push(req);
          
        }
        return forkJoin(getarray)
          .pipe(
            map(this.extractData),
            catchError(this.handleError)
          )
          .toPromise();
      
    }
    else
    {

      let date7  = new Date(to);
      let date6 = new Date(this.datepipe.transform(date7, 'yyyy-MM-dd'));
      date6.setDate(date7.getDate()-1);
      let date5 = new Date(this.datepipe.transform(date6, 'yyyy-MM-dd'));
      date5.setDate(date6.getDate()-1);
      let date4 = new Date(this.datepipe.transform(date5, 'yyyy-MM-dd'));
      date4.setDate(date5.getDate()-1);
      let date3= new Date(this.datepipe.transform(date4, 'yyyy-MM-dd'));
      date3.setDate(date4.getDate()-1);
      let date2 = new Date(this.datepipe.transform(date3, 'yyyy-MM-dd'));
      date2.setDate(date3.getDate()-1);
      let date1 = new Date(this.datepipe.transform(date2, 'yyyy-MM-dd'));
      date1.setDate(date2.getDate()-1);


        let datestr7 = date7.getFullYear()+"-"+(date7.getMonth()+1)+"-"+date7.getDate();
        let datestr6 = date6.getFullYear()+"-"+(date6.getMonth()+1)+"-"+date6.getDate();
        let datestr5 = date5.getFullYear()+"-"+(date5.getMonth()+1)+"-"+date5.getDate();
        let datestr4 = date4.getFullYear()+"-"+(date4.getMonth()+1)+"-"+date4.getDate();
        let datestr3 = date3.getFullYear()+"-"+(date3.getMonth()+1)+"-"+date3.getDate();
        let datestr2 = date2.getFullYear()+"-"+(date2.getMonth()+1)+"-"+date2.getDate();
        let datestr1 = date1.getFullYear()+"-"+(date1.getMonth()+1)+"-"+date1.getDate();

        let day7 = this.http.get(this.baseURL+"/"+this.urls.workcenters+"/"+datestr7, { responseType: 'text' });//(this.baseURL+"/"+this.urls.workcenters+"/"+date7)
        let day6 = this.http.get(this.baseURL+"/"+this.urls.workcenters+"/"+datestr6, { responseType: 'text' });//(this.baseURL+"/"+this.urls.workcenters+"/"+date6)
        let day5 = this.http.get(this.baseURL+"/"+this.urls.workcenters+"/"+datestr5, { responseType: 'text' });//(this.baseURL+"/"+this.urls.workcenters+"/"+date5)
        let day4 = this.http.get(this.baseURL+"/"+this.urls.workcenters+"/"+datestr4, { responseType: 'text' });//(this.baseURL+"/"+this.urls.workcenters+"/"+date4)
        let day3 = this.http.get(this.baseURL+"/"+this.urls.workcenters+"/"+datestr3, { responseType: 'text' });//(this.baseURL+"/"+this.urls.workcenters+"/"+date3)
        let day2 = this.http.get(this.baseURL+"/"+this.urls.workcenters+"/"+datestr2, { responseType: 'text' });//(this.baseURL+"/"+this.urls.workcenters+"/"+date2)
        let day1 = this.http.get(this.baseURL+"/"+this.urls.workcenters+"/"+datestr1, { responseType: 'text' });//(this.baseURL+"/"+this.urls.workcenters+"/"+date1)

        if (workcenter!="")
        {
           
           day7 = this.http.get(this.baseURL+"/"+this.urls.workcenter+"/"+datestr7+"/"+workcenter, { responseType: 'text' });//(this.baseURL+"/"+this.urls.workcenters+"/"+date7)
           day6 = this.http.get(this.baseURL+"/"+this.urls.workcenter+"/"+datestr6+"/"+workcenter, { responseType: 'text' });//(this.baseURL+"/"+this.urls.workcenters+"/"+date6)
           day5 = this.http.get(this.baseURL+"/"+this.urls.workcenter+"/"+datestr5+"/"+workcenter, { responseType: 'text' });//(this.baseURL+"/"+this.urls.workcenters+"/"+date5)
           day4 = this.http.get(this.baseURL+"/"+this.urls.workcenter+"/"+datestr4+"/"+workcenter, { responseType: 'text' });//(this.baseURL+"/"+this.urls.workcenters+"/"+date4)
           day3 = this.http.get(this.baseURL+"/"+this.urls.workcenter+"/"+datestr3+"/"+workcenter, { responseType: 'text' });//(this.baseURL+"/"+this.urls.workcenters+"/"+date3)
           day2 = this.http.get(this.baseURL+"/"+this.urls.workcenter+"/"+datestr2+"/"+workcenter, { responseType: 'text' });//(this.baseURL+"/"+this.urls.workcenters+"/"+date2)
           day1 = this.http.get(this.baseURL+"/"+this.urls.workcenter+"/"+datestr1+"/"+workcenter, { responseType: 'text' });//(this.baseURL+"/"+this.urls.workcenters+"/"+date1)

        }

      
      return forkJoin([day1, day2, day3, day4, day5, day6, day7])
      .pipe(
        map(this.extractData),
        catchError(this.handleError)
      )
      .toPromise();
    }

  
  }

  getWorkcenter(from: string, to: string, workcenter: string): Promise<any> {

    let fromdate = new Date(from);
    let todate = new Date(to);
    
    let fromdatestr = fromdate.getFullYear()+"-"+(fromdate.getMonth()+1)+"-"+fromdate.getDate();
    let todatestr = todate.getFullYear()+"-"+(todate.getMonth()+1)+"-"+todate.getDate();
    
    if (workcenter=="")
    {
      return this.http.get(this.baseURL+"/"+this.urls.workcenters+"/"+fromdatestr+"/"+todatestr, { responseType: 'text' })//(this.baseURL+"/"+this.urls.workcenters+"/"+fromdate+"/"+todatestr)
        .pipe(
            map(this.extractData),
            catchError(this.handleError)
          )
          .toPromise();
    }
    else
    {
      return this.http.get(this.baseURL+"/"+this.urls.workcenter+"/"+fromdatestr+"/"+todatestr+"/"+workcenter, { responseType: 'text' })//(this.baseURL+"/"+this.urls.workcenter+"/"+fromdate+"/"+todatestr)
      .pipe(
          map(this.extractData),
          catchError(this.handleError)
        )
        .toPromise();
      }
  }

  getAllJobsAchievement(from: string, to: string): Promise<any> {

    let fromdate = new Date(from);
    let todate = new Date(to);
    
    let fromdatestr = fromdate.getFullYear()+"-"+(fromdate.getMonth()+1)+"-"+fromdate.getDate();
    let todatestr = todate.getFullYear()+"-"+(todate.getMonth()+1)+"-"+todate.getDate();
    
     return this.http.get(this.baseURL+"/"+this.urls.jobs+"/"+fromdatestr+"/"+todatestr, { responseType: 'text' })
    .pipe(
        map(this.extractData),
        catchError(this.handleError)
      )
      .toPromise();
  }

  private extractData(res: Response | any) {
    return res;
  }

  private handleError(err: Response | any) {
    if (!err.Message) {
      err.Message = err.statusText;
    }
    return Observable.throw(err.Message || err);
  }

}
